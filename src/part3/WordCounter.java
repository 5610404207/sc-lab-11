package part3;

import java.util.HashMap;

public class WordCounter {
	String message;
	public HashMap<String,Integer> wordCount;
	String[] m;
	
	WordCounter(String message){
		this.message = message;
		wordCount = new HashMap<String,Integer>();
	}
	public void count(){
		m = message.split(" ");
		for (int i=0 ; i<m.length ; i++){
			if (wordCount.keySet().contains(m[i])){
				int newnum = wordCount.get(m[i])+1;
				wordCount.replace(m[i], newnum);
			}else{
				wordCount.put(m[i],1);
			}
		}
	}
	public int hasWord(String word){
		int count = 0;
		for (int i=0 ; i<m.length ; i++){
			if (wordCount.containsKey(word)){
				count = wordCount.get(word);
		}
	}
		return count;
}
}
