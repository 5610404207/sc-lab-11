package part5;

public class RefrigeratorMain {

	public static void main(String[] args) {
		Refrigerator ref = new Refrigerator();
		ref.put("Vegetable");
		ref.put("Egg");
		ref.put("Meat");
		ref.put("Milk");
		ref.put("Cake");

		ref.put("Ice");
		ref.put("Tea");

		System.out.println(ref.toString());
		System.out.println("Take Out Egg :" + ref.takeOut("Egg"));
		System.out.println("Take Out Tea : " + ref.takeOut("Tea"));
		System.out.println("Take Out Coffee :" + ref.takeOut("Coffee"));
	}

}
