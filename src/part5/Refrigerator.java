package part5;

import java.util.ArrayList;

public class Refrigerator {

	int size = 5;
	int enter = 1;
	ArrayList<String> things = new ArrayList<String>();

	public void put(String stuff) {
		try {
			if (enter <= 5) {
				things.add(stuff);
				enter++;
			} else {
				throw new FullException();
			}
		} catch (FullException e) {
			System.err.println("refrigerator is full");
		}
	}

	public String takeOut(String stuff) {
		if (things.contains(stuff)) {
			return stuff;
		} else {
			return null;
		}
	}

	public String toString() {
		return things.toString();
	}

}