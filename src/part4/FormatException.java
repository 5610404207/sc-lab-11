package part4;

public class FormatException extends Exception {
	
	public FormatException() {
	}

	public FormatException(String message) {
		super(message);
	}
}
