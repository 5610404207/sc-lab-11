package part4;

public class MyClass {

	public static void main(String[] args) {
		try {
			MyClass c = new MyClass();
			System.out.println("A");
			c.methX();
			System.out.println("B");
			c.methY();
			System.out.println("C");
			return;
		} catch (DataException e) {
			System.out.println("D");
		} catch (FormatException e) {
			System.out.println("E");
		} finally {
			System.out.println("F");
		}
		System.out.println("G");
	}

	public void methX() throws DataException {///no throwing error
		throw new DataException();
	}

	public void methY() throws FormatException {///no throwing error
		throw new FormatException();/// error at throws
	}
}

	///Answer Question 2 Unreachable code at System.out.println("G");
	///Answer Question 3 ADFG
	///Answer Question 4 ABEFG